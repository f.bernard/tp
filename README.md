___IMPORTANT NOTES___

L'évaluation se fera sur un entretien individuel de 5 à 10 minutes par personne durant la dernière séance. 
Chaque note sera différente pour les personnes d'un même groupe en fonction des critères données à titre 
indicatif ci-dessous. Ainsi, chaque personne doit pouvoir présenter le projet du groupe et plus particulièrement 
les aspects suivants:

- L'état du dépôt et la partie administrative du projet (Comment se sont passées les 
    communications, les issues, la répartition du travail, la structure du dépôt)
- La description du projet et des choix effectués, plus les contributions 
    personnelles de chacun (qu'est ce qui a été fait dans le projet, pour quoi ? 
    pourquoi ? Et quelle est la contribution de chacun et plus particulièrement 
    de la personne qui présente).
- Le projet. Il faut montrer la production du groupe. (Quelle est la production 
    du groupe ? Il faut mieux avoir une ou deux fonctionnalités abouties que 5 
    fonctionnalités à peine développées). 


<br><br><br><br><br><br><br><br><br><br>




## The project

__READ THE BOOSTRAPTING AND PROJECT SESSION__

#### Organisation. 

Project will be done in group of 3 persons. Each person should belong to a different
"TP" group. You may ask why ? The reason is experimental. We (Mr Turquay and Myself) 
would like to see how you behave in this kind of groups. The characteristic on this
scheme are: 1) You should communicate with your team-mate in a different manner. 2)
You should use appropriate tools to work and share. 3) You have to split the work (which
is not always the case in classical group formation). 4) ...

#### The project

The project is: ___RT Key Managment___. You have to manage the keys of the R&T department.
This means that :

- At anytime, it should be possible to localize the keys. (We assume that they stay within the IUT campus)
- If a key battery is to low, it should raise an alert. (You choose the alert)
- We should be able to know who borrowed the keys (use your imagination, implements some mechanisms).
- Keys are also a way to monitor the RT departement with other sensors (cooling system, ...) 
- Key states (localisation, sensor states, ...) should be easily accessible.

#### Bootstraping

The project can be huge if you look at the big picture. You should be able to split it
in order to tackle small pieces at a time. But in order to help you in that process,
we will give you some hints about how to split and where you should be after each "TP" 
session. These are just indications, feel free "NOT" to follow them. The only important
things are the `commits` and `tags` at the end of your working session. So please do not 
forget that part.

- Project Session 1: [here](https://gitlab.com/m4207/tp/blob/master/tp1.md) - online
- Project Session 2: [here](https://gitlab.com/m4207/tp/blob/master/tp2.md) - online
- Project Session 3: [here](https://gitlab.com/m4207/tp/blob/master/tp3.md) - online
- Project Session 4: [here](https://gitlab.com/m4207/tp/blob/master/tp4.md) - online
- Project Session 5: [here](https://gitlab.com/m4207/tp/blob/master/tp5.md) - online
- Project Session 6: [here](https://gitlab.com/m4207/tp/blob/master/tp6.md) - online

#### Side notes

You should use everything you've learned so far... 