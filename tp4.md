## Project session 4

This session is related to best pratices in your project. So far, we have let you to
whatever you wanted to do in order to let you kick-start the project and have some
runing code as fast as possible.

Now that you have code and you have you workflow, let us introduce good pratices.
You are going to learn how to use the full power of GitLab (the same apply for github).

### Preliminaries

Depending on your machine this is the way to install git:
- linux ubuntu: apt-get install git vim
- mac osx: https://git-scm.com/
- windows: https://git-for-windows.github.io/

For all the version git can be used via a command line interface. Note that for
the windows version, a bash is specifically install with `vim`. You can
increase your git experience by using a GUI but it is not mandatory. 
there are a lot of them available for different platform. There is a list 
[here](https://git-scm.com/downloads/guis). A good alternative is: 
[SourceTree](https://www.sourcetreeapp.com/). A GUI help visializing the state 
of your repository.

Please carefully follow the instalation instruction for both software.

In Sourcetree, once instaled, you can add a repository from URL. Use the
terminal if you feel more confortable with it. This is just the instalation
part. We will come back to SourceTree usage later.

### Setup

If you have not done it, please create a group for your project, and invite your teammates to the group.
Also invite Mr Turquay and I.

To create a group:
- use the `burger menu` on the top-left of the gitlab page and click on `groups`.
- click on the green button on the top right `new group`
- Fill the form by giving a name to your group and make it public or private, it's up to you. click on the green button `create group`on the bottom-left
- Once the group is created, you should be `connected` to the group. The name of the group should appear on the top-left.
- Click on the `members` menu (below the gitlab logo) and add members your teammates and Mr Turquay: dtu and me: trazafin. Add your teammates as owner and Mr Turquay and I as Developer.
- Click on the `group` menu (below the gitlab logo) and create a repository, you can call it `m4207-project` or `LinkitOne-KeyManagement`, it's s up to you but choose carefully. Note that you can create as many repository as you want. __Different repositories are useful only if you are using gitlab with the same person, for different things. In our case create only one repository__


You now have a project. Create a readme.md file just to have something on the repository of your group.

In you project, create a _good_ directory structure. This is an example of the minimal structure (I think):
```bash
    Rep
    ├── src
        ├── localization_test.ino
        └── gps_test.ino
    ├── doc
        ├── how_to_compile.md
        └── list_of_useful_links.md
    ├── journal.md
    └── readme.md
```

- The `src` directory should contain your code.
- The `doc` directory should contain files with the links you visited, or pdf, etc...
- The `journal.md` file should contain the your day to day work. Members of the group should append it after each session. It should contain a description or link to the code (code), a description of what have been done or a link to an issue (done part), and some link to some issues (expectation part)
- The `readme.md` should contain a short description of the project. At least which is the name of the main file corresponding to your project (to compile), the sensors taht should be connect and on which ports, etc... In fact all information that helps run your project.

### Using the full power of git (gitlab or github)

Let us exploit the issues menu. Click on the `issues` menu (just below the gitlab logo).
You are able to see a sub-menu containing `issues, board, label, milestones`.

#### Step 0: Discovering.

- Open the `board` sub-menu. If not done yet, you can clicl on the `add default list` on the middle right.
    This will create some default boards and related labels. We will get back on this later.
- Open the `labels` sub-menu. You can create some label related to the project for example some useful labels are :
    `localization`, `battery`, `userId`, `bugs`, `refactoring` ...


#### Step 1: Opening an issue.

- Open the `issues` sub-menu. You can create an issue by clicking on the green button middle-right.
    An issue is an atomic task, or atomic _problem_.
    In a real development process it's an half-day task. In our cacse, let us say that it's at
    most a 3 hours task, but it's more rewarding if it's a 1 hours (or even 1/2 hour task).
    - Let us, for example, create an issue called: `testing precision of the localization technique`
    - In the description write exactly what you expect (for example): `get 100 gps positions in front of each door, average them, and compare them to see if there is a difference`
    - You can and should assign the issue to someone (can be yourself).
    - You must assign one or more labels, in our case: `To Do` and `localization`.
    - submit the issue.


#### Step 2: The board

Go to the `board` sub-menu. (the board is a kind of kanban, google it you want to know more).
If everything is done correctly (from step 0), you should have 3 boards: `To Do`, `Doing`, `Done`.
Except for the `Done` panel, the other panels are named after the tags. These are the default settings and they are quite good.

You can populate the board by adding an issue (top right button). Note that the issue should be create beforehand.
However, the panel should be automatically populated based on the isssue's label.

Once an issue is in the board, you can drag and drop it from one panel to another (the label will automalically change).

#### Step 3: Your workflow

Before doing anything related to your code, you should open an issue. This helps you to keep track of your
work and the things you have in mind. Your simplified workflow should be as follow (simplified since we are not going to branch).
- Create an issue, put correct tag, assignee, etc...
- Work on a specific issue, place it to the `Doing` panel
- Commit you code, changes, etc... In your commit message put: Closes #3 (3 is the issue number, verify your issue number.)
- Close the issue (move it to the done panel)

#### Step 4: Code and Conquer the world

Get the things done. Use GIT, VIM, ...  Discover SourceTree.
