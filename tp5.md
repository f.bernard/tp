## Project session 5

#### Where should you be ? 

You have already implemented some basic localization and alert techniques, you should
know how to implement the identification.

Communication inside your group should be completely operationnal and using the gitlab
group should now be your primary focus. 

#### Solutions or help for the n-2 session

If you are still stucked into the localization process or alarm, __you're doomed__. Contact the professor. 
Please look at the tips 
provided in the previous sessions [here](https://gitlab.com/m4207/tp/blob/master/tp2.md) and 
[here](https://gitlab.com/m4207/tp/blob/master/tp3.md). If you do not 
know how to use the GPS, you should give a look at the Arduino IDE example. For those who want to increase
precision by using WiFi, ___this is a great and good thing but you should not spend so many time on this___. 
The files regarding the WiFi architecture of R&T departement are [here](https://gitlab.com/m4207/tp/tree/master/files)

__In any case, you should move on__


#### Solutions or help for the n-1 and n-2 sessions

Please see the previous sections. Alert you be ok for now. ___DO NOT SPEND TIME DEPLEATING THE BATTERY___ to
test your code. Find another way or do not test it since could take you days to do that.

For example, you can raise an alarm depending on the position

#### What should you do in this session ? 

Go to the dark side of the force, enjoy free foods and cookies and conquer the world to be the master of the universe. 

You still have to answer this question: 
- __Who takes the key ?__ 

You should be able or the key should be able to know who takes the key. Basic identification is the 
student number. This information should be kept somewhere. The difficult part here is to sent the 
information from a mobile phone, a PC or whatever means you have and to make the key receive this 
information. 

___YOU MUST RAISE AN ISSUE EXPLAINING THE WAY YOU ARE GOING TO DO THIS___


#### Tips

There are a lot of ways to send an information to the key, if you have a sim card for example, drop the key an
SMS. Or the best way and since you are in the Networking and Telecommunication department, use a network.

You could use bluetooth (hard), you can use QR-code or external mean (difficult), connect the linkitone to a web 
server that will act as a broker between the client and the key (difficult), ...

